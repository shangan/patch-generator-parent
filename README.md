# patch-generator-parent
通用svn、git增量部署补丁生成器，可用于svn/git/getee管理的maven项目的增量部署文件生成，支持多模块父子项目.

该模块可以对git/svn管理的项目进行增量代码生成用于增量发版。总共包含四种生成方案：


- 1、git服务器分支提交分析；
- 2、git提交日志分析；
- 3、svn服务器分支提交分析；
- 4、svn提交日志分析；

 **项目规划** ：empire团队后期将提供桌面版的补丁打包工具，敬请期待！

 **郑重声明**：本软件原为emsite配套核心代码，目前已开启独立发展之路！但凡软件对您有一点帮助，请为项目加星，点赞！非常感谢！

 **相关项目** 

[emsite分布式开源系统生态圈](https://gitee.com/hackempire/emsite-parent)

[SVNLog](https://github.com/78425665/SVNLog)

[jenkins_increse_plugin](https://github.com/don-cat/jenkins_increse_plugin)


 **联系方式** 

QQ群：456742016（empire架构组）